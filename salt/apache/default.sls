{% set fqdn = salt['grains.get']('fqdn') %} # the servers fqdn
{% set mem = salt['grains.get']('mem_total') %} # the servers total memory
{% set settings = salt['pillar.get']('apache') %} # as defined in pillars/apache/
{% set ram_base = (((mem / 1000)|round) +1)|int %}
{% set max_spare_servers = (ram_base + 1)|int %}
{% set server_limit = ( 50 + ((ram_base ** 2) * 10 ) + ((ram_base - 2)*10) )|int %}
{% set max_requests_per_child = ( 2048 + (ram_base * 256) )|int %}
include:
  - apache.packages

{{ settings.default_host_root }}:
  file.directory:
    - user: root
    - group: root
    - dir_mode: '0755'
    - makedirs: True # Like running makedir -p
    - require:
      - pkg: apache2

{{ settings.default_host_log }}:
  file.directory:
    - user: root
    - group: adm
    - dir_mode: '0750'
    - makedirs: True # Like running makedir -p
    - require:
      - pkg: apache2

/etc/apache2/mods-available/mpm_prefork.conf:
  file.managed:
    - source: salt://filebase/apache/mpm_prefork.conf
    - user: root
    - group: root
    - mode: 0644
    - template: jinja
    - watch_in:
      - service: apache2
    - defaults:
      mem: {{ mem }}
      ram_base: {{ ram_base }}
      max_spare_servers: {{ max_spare_servers }}
      server_limit: {{ server_limit }}
      max_requests_per_child: {{ max_requests_per_child }}
 
/etc/logrotate.d/apache2:
  file.managed:
    - source: salt://filebase/apache/logrotate # will generate the logrotate file also for the websites
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - pkg: apache2
      - file: {{ settings.default_host_log }}
    - template: jinja
    - defaults:
      settings: {{ settings }}
      fqdn: {{ settings }}
 
/etc/apache2/sites-available/000-default.conf:
  file.managed:
    - source: salt://filebase/apache/site_available_template.conf # will also be used in salt/apache/websites.sls
    - mode: '0644'
    - user: root
    - group: root
    - template: jinja
    - watch_in:
      - service: apache2
    - require:
      - pkg: apache2
      - file: {{ settings.default_host_root }}
      - file: {{ settings.default_host_log }}
    - defaults:
      fqdn: {{ fqdn }}
      settings: {{ settings }}
      website: False

/etc/ssl/salt-managed/certs:
  file.directory:
    - user: root
    - group: root
    - dir_mode: '0755'
    - makedirs: True

/etc/ssl/salt-managed/keys:
  file.directory:
    - user: root
    - group: root
    - dir_mode: '0700'
    - require:
      - file: /etc/ssl/salt-managed/certs

/etc/apache2/sites-available/default-ssl.conf:
  file.absent: # this will delete the file
    - watch_in:
      - service: apache2

/etc/apache2/sites-enabled/default-ssl.conf:
  file.absent: # this will delete the symlink
    - watch_in:
      - service: apache2

{% if settings.ssl_enable_bundle %}
/etc/ssl/salt-managed/bundle.crt:
  file.managed:
    - source: salt://filebase/apache/ssl/bundle.crt
    - require:
      - file: /etc/ssl/salt-managed/certs
    - watch_in:
      - service: apache2
{% endif %}
 
{% if settings.default_host_enable_ssl %}
/etc/ssl/salt-managed/certs/{{ fqdn }}.crt:
  file.managed:
    - source: salt://filebase/apache/ssl/certs/{{ fqdn }}.crt
    - user: root
    - group: root
    - mode: 0644
    - watch_in:
      - service: apache2
 
/etc/ssl/salt-managed/keys/{{ fqdn }}.key:
  file.managed:
    - source: salt://filebase/apache/ssl/keys/{{ fqdn }}.key
    - user: root
    - group: root
    - mode: 0400
    - watch_in:
      - service: apache2
{% endif %}
